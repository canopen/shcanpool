#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

readonly PROG="demo"
readonly DESC="Shell Command Framework Demo."
readonly VERSION="0.0.1"
readonly CONFIG_FILE="$HOME/.$PROG.conf"
