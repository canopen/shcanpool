#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

method_def uncompress


usage_uncompress() {
printf "uncompress (uc): Uncompress commonly used compressed packages

usage:
    ${PROG} uc FILE
\n"
}

alias_def uncompress uc
# do_uncompress FILE
do_uncompress() {
    if [[ -z "$1" || ! -e "$1" ]]; then
        usage_uncompress; exit
    fi
    bash ${BASH_SOURCE[0]%/*}/uncompress.bash $@
}
