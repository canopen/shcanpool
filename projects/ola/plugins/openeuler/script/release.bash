#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0


NAME=${0##*/}

# commands
cmd=""

# argumentss
args=""
arg_html=""
arg_url=""
arg_filerpm=""

usage() {
printf "release: A tool for obtaining information about openeuler releases

usage:
    bash $0 [OPTIONS...]

options and args:
    -h, --help              Show this help message and exit
    -e, --example           Show this examples and exit
    -l, --list              List the released rpms
    -r, --rversion          Get the version informations of release rpms
    -d, --download FILE     Download the rpm listed in FILE
    -u, --downunpack FILE   Download and unpack the rpm listed in FILE
        --url PATH          Specify the repo url path of the rpm package
        --html FILE         Specify the local html file (wget url path to get it)
\n"
}

usage_help() {
    echo "release: try -h/--help or -e/--example for more details"
}

usage_example() {
printf "$(usage_help)

examples:
    1) get the list of released RPM
    bash $NAME -l --url https://repo.openeuler.org/openEuler-22.03-LTS/source/Packages/
    bash $NAME -l --html index.html

    2) get the versions of released RPM
    bash $NAME -r --url https://repo.openeuler.org/openEuler-22.03-LTS/source/Packages/
    bash $NAME -r --html index.html

    3) download the rpm listed in FILE
    bash $NAME -d FILE --url https://repo.openeuler.org/openEuler-22.03-LTS/source/Packages/

    4) download and unpack the rpm listed in FILE
    bash $NAME -u FILE --url https://repo.openeuler.org/openEuler-22.03-LTS/source/Packages/
\n"
}

get_option() {
    local ARGS=$(getopt -o "helrd:u:" \
        -l "help,example,list,rversion,download:,downunpack:,url:,html:" \
        -n "release" -- "$@")
    eval set -- "${ARGS}"

    while true; do
        case "${1}" in
            -h|--help)
                usage; exit
                ;;
            -e|--example)
                usage_example; exit
                ;;
            -l|--list)
                cmd="list"; shift 1
                ;;
            -r|--rversion)
                cmd="rversion"; shift 1
                ;;
            -d|--download)
                cmd="download"; arg_filerpm="$2"; shift 2
                ;;
            -u|--downunpack)
                cmd="downunpack"; arg_filerpm="$2"; shift 2
                ;;
            --url)
                arg_url="$2"; shift 2
                ;;
            --html)
                arg_html="$2"; shift 2
                ;;
            --)
                shift; break
                ;;
        esac
    done
    args="$@"
}

do_list() {
    local html=$arg_html
    local rpms=""
    if [ -n "$html" ]; then
        if [ -f "$html" ]; then
            rpms=$(cat "$html")
        fi
    else
        html=$arg_url
        if [ -n "$html" ]; then
            rpms=$(curl -s -X GET "$html")
        fi
    fi
    if [[ -z "$rpms" ]]; then
        echo "get package failed from \"$html\""
        usage_help; exit
    else
        rpms=$(echo "$rpms" | grep -oE 'title="[^"]+' | grep -E "rpm$" | cut -d '"' -f 2 | sort -u)
        echo "$rpms"
    fi
}

do_rversion() {
    local rpms="$(do_list)"
    echo "package,version,release"
    for rpm in $rpms; do
        local release=${rpm##*-}
        release=$(echo "$release" | cut -d . -f 1)
        local name_version=${rpm%-*}
        local name=${name_version%-*}
        local version=${name_version##*-}
        if [[ "$version" =~ 0$ ]]; then
            version="'$version"
        fi
        echo "$name,$version,$release"
    done
}

do_download() {
    if [[ ! -f "$arg_filerpm" || -z "$arg_url" ]]; then
        usage_help; exit
    fi
    local pkgs=$(cat "$arg_filerpm")
    for pkg in $pkgs; do
        local url=${arg_url}${pkg}
        [[ -f "$pkg" ]] && rm -f $pkg
        wget $url --no-check-certificate
        if [ $? -ne 0 ]; then
            echo "wget $url failed"; continue
        fi
    done
}

do_downunpack() {
    if [[ ! -f "$arg_filerpm" || -z "$arg_url" ]]; then
        usage_help; exit
    fi
    local pkgs=$(cat "$arg_filerpm")
    for pkg in $pkgs; do
        local name=$(echo "$pkg" | sed -r 's/-[^-]+-[^-]+$//')
        local url=${arg_url}${pkg}
        if [[ -d "$name" && "$(realpath "$name")" != "/" ]]; then
            rm -rf "$name"
        fi
        mkdir $name
        cd $name &>/dev/null
        wget $url --no-check-certificate
        if [ $? -ne 0 ]; then
            echo "wget $url failed"; continue
        fi
        rpm2cpio $pkg | cpio -id
        cd - &>/dev/null
    done
}

main() {
    get_option "$@"

    case "$cmd" in
        "list")
            do_list
            ;;
        "rversion")
            do_rversion
            ;;
        "download")
            do_download
            ;;
        "downunpack")
            do_downunpack
            ;;
        *)
            usage_help
            ;;
    esac
}

main "$@"
