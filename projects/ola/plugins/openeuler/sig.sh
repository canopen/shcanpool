#!/usr/bin/env bash
# Copyright (c) 2021-2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

method_def sig


usage_sig() {
    module_usage "sig" "Analyze the SIG (Special Interest Group)"
}

do_sig() {
    module_do "sig" "$@"
}

sig_usage_help() {
    module_usage_help sig
}

# sig_do_help subcmd
sig_do_help() {
    module_do_help sig "$1"
}

__sig_get_subcmd() {
    local cmd=""
    case "${1}" in
        "h"|"help")
            cmd="help"
            ;;
        "l"|"ls"|"list")
            cmd="list"
            ;;
        "p"|"pkg"|"package")
            cmd="package"
            ;;
        "o"|"owner")
            cmd="owner"
            ;;
        "f"|"find")
            cmd="find"
            ;;
        "m"|"maintainer")
            cmd="maintainer"
            ;;
        *)
            ;;
    esac
    echo "$cmd"
}

sig_list() {
    local sigs=$(gec_get_path openeuler community sig master)
    sigs=$(echo "$sigs" | awk -F ',' '{for (i=1;i<=NF;i++) {print $i}}' |\
        grep -E '"name"' | grep -viE "*.(md|py)" | awk -F ':' '{print $2}' | sed 's/"//g')
    echo "$sigs"
}

# sig_find pkg
sig_find() {
    local sigs=$(gec_get_project_labels src-openeuler "$1")
    echo "$sigs"
}

# sig_info signame
sig_info() {
    curl -s -X GET "https://gitee.com/openeuler/community/raw/master/sig/${1}/sig-info.yaml"
}

# sig_onwers signame
sig_onwers() {
    curl -s -X GET "https://gitee.com/openeuler/community/raw/master/sig/${1}/OWNERS"
}

# sig_get
sig_get() {
    curl -s -X GET "https://gitee.com/openeuler/community/raw/master/sig/sigs.yaml"
}


# sig_find_owner sig
sig_find_owner() {
    local signame=$1
    local sigowner=$(sig_onwers "$signame")
    local owners=$(echo "$sigowner" | grep '^\s*-\s*' | sed 's/^\s*-\s*//')
    if [ -z "$owners" ]; then
        owners="Not-Found"
    fi
    echo "$owners"
}

sig_usage_owner() {
printf "owner (o): Find the owner of sig

usage:
    ${PROG} sig o SIGNAME
\n"
}

# sig_do_owner sig
sig_do_owner() {
    if [ $# -ne 1 ]; then
        sig_usage_owner; exit
    fi
    sig_find_owner "$1"
}


sig_usage_list() {
printf "list (l, ls): List all sigs

usage:
    ${PROG} sig ls
\n"
}

sig_do_list() {
    sig_list
}


# sig_find_package sig
sig_find_package() {
    local signame=$1
    local siginfo=$(sig_info "$signame")
    local pkgs=$(echo "$siginfo" | grep -E "^- repo:" | awk -F ':' '{print $2}' | sed 's/ //g')
    if [ -z "$pkgs" ]; then
        echo "$siginfo"
    else
        echo "$pkgs"
    fi
}

sig_usage_package() {
printf "package (p, pkg): Find the packages of sig

usage:
    ${PROG} sig pkg SIGNAME
\n"
}

# sig_do_package sig
sig_do_package() {
    if [ $# -ne 1 ]; then
        sig_usage_package; exit
    fi
    sig_find_package "$1"
}


sig_usage_find() {
printf "find (f): Find the sig group of packages

usage:
    ${PROG} sig f PACKAGES         # package list
    ${PROG} sig f FILE             # list file

example:
    ${PROG} sig f \"pkgship python3 a b c\"
\n"
}

# sig_do_find pkgs
# sig_do_find file
sig_do_find() {
    if [ $# -lt 1 ]; then
        sig_usage_find; exit
    fi
    local pkgs=$(get_list "$1")
    {
    printf "%s|%s\n" "SIG" "PKG"
    printf "%s|%s\n" "--------" "--------"
    for pkg in $pkgs; do
        local sig=$(sig_find "$pkg")
        if [ -z "$sig" ]; then
            sig="Not-Found"
        fi
        printf "%s|%s\n" "$sig" "$pkg"
    done | sort -nk1
    } | format_column '|'
}

sig_usage_maintainer() {
printf "maintainer (m): Find the maintainer of sig

usage:
    ${PROG} sig m SIGNAME
\n"
}

# sig_do_maintainer sig
sig_do_maintainer() {
    if [ $# -ne 1 ]; then
        sig_usage_maintainer; exit
    fi
    local signame=$1
    local siginfo=$(sig_info "$signame")
    local maintainers=$(echo "$siginfo" | sed -r -e '1,/^maintainers:/d' -e '/^repositories:/,$d')
    if [ -z "$maintainers" ]; then
        echo "$siginfo"
    else
        echo "$maintainers"
    fi
}
