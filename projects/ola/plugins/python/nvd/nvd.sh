#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

method_def nvd

__NVD_PY="${BASH_SOURCE[0]%/*}/nvd.py"

usage_nvd() {
printf "nvd: Get the CVE informations from NVD (csv format)

usage:
    ${PROG} nvd [-k|--keyword KEYWORD] [-c|--cve CVE]

optional arguments:
  --keyword KEYWORD  The keyword of soft package. If the KEYWORD is multiple words,
                     please use + to connect, such as \"a b\" -> \"a+b\"
  --cve CVE          The cve file or list
\n"
}

do_nvd() {
    local ARGS=$(getopt -o "k:c:" -l "keyword:,cve:" -n "nvd" -- "$@")
    eval set -- "${ARGS}"

    while true; do
        case "${1}" in
            -k|--keyword)
                python3 $__NVD_PY -k "${2}"
                return
                ;;
            -c|--cve)
                python3 $__NVD_PY -c "${2}"
                return
                ;;
            --)
                shift; break
                ;;
            *)
                break
                ;;
        esac
    done
    usage_nvd
}
