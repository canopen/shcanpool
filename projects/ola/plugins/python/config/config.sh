#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0


__CONFIG_PY="${BASH_SOURCE[0]%/*}/config.py"


unset -f config_get
# config_get file section key
config_get() {
    python3 $__CONFIG_PY -c get -f "$1" -s "$2" -k "$3"
}

# config_get file section key value
config_set() {
    python3 $__CONFIG_PY -c set -f "$1" -s "$2" -k "$3" -v "$4"
}
