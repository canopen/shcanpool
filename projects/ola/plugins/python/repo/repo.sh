#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

method_def repo

__REPO_PY="${BASH_SOURCE[0]%/*}/repo"

usage_repo() {
printf "repo: The Multiple Git Repository Tool (git-repo)

usage:
    ${PROG} repo ...

try \"${PROG} repo -h/--help\" for more
\n"
}

do_repo() {
    export REPO_URL="https://github.com/GerritCodeReview/git-repo.git"
    python3 $__REPO_PY $@
    unset REPO_URL
}
