#!/usr/bin/env bash
# Copyright (c) 2021-2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0


# python_path_add path
python_path_add() {
    local path=$1
    if [ ! -d "$path" ]; then
        echo "arg is not path"; exit
    fi
    local python_paths=$(echo ${PYTHONPATH} | sed 's/:/ /g')
    local existed=0
    for p in $python_paths; do
        if [[ "$p" = "$path" ]]; then
            existed=1; break
        fi
    done

    if [[ $existed -eq 0 ]]; then
        if [[ -z "${PYTHONPATH}" ]]; then
            export PYTHONPATH=${path}
        else
            export PYTHONPATH=${PYTHONPATH}:${path}
        fi
    fi
}

python_docheck() {
    check_command "python3"
}
