#!/usr/bin/env bash
# Copyright (c) 2021-2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

current_path=$(dirname $(readlink -f "$0"))

readonly PROJECT_DIR="$current_path/.."
readonly SHCANPOOL_DIR="$PROJECT_DIR/../.."

source $SHCANPOOL_DIR/src/base/load.sh

load $SHCANPOOL_DIR/src/utils
load $SHCANPOOL_DIR/src/libs
load $SHCANPOOL_DIR/src/misc
load $SHCANPOOL_DIR/src/plugins
load $SHCANPOOL_DIR/src/apps
load $SHCANPOOL_DIR/test/libs
load $SHCANPOOL_DIR/test/case

load $PROJECT_DIR/libs
load $PROJECT_DIR/plugins
load $PROJECT_DIR/apps
load $PROJECT_DIR/test/case

main() {
    ut_run "$@"
}

main "$@"
