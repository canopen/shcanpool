#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

# test_spec_get_version file
test_spec_get_version() {
    local spec=$1
    if [ ! -f "$spec" ]; then
        echo "Usage: $FUNCNAME file"; return
    fi
    spec_get_version $spec
}

# test_spec_get_release file
test_spec_get_release() {
    local spec=$1
    if [ ! -f "$spec" ]; then
        echo "Usage: $FUNCNAME file"; return
    fi
    spec_get_release $spec
}

# test_spec_parse file
test_spec_parse() {
    local spec=$1
    if [ ! -f "$spec" ]; then
        echo "Usage: $FUNCNAME file"; return
    fi
    spec_parse $spec
}

# test_spec_get_changelog file
test_spec_get_changelog() {
    local spec=$1
    if [ ! -f "$spec" ]; then
        echo "Usage: $FUNCNAME file"; return
    fi
    spec_get_changelog "$spec"
}

# test_spec_get_developer file
test_spec_get_developer() {
    local spec=$1
    if [ ! -f "$spec" ]; then
        echo "Usage: $FUNCNAME file"; return
    fi
    spec_get_developer "$spec"
}

# test_spec_get_developer_email file
test_spec_get_developer_email() {
    local spec=$1
    if [ ! -f "$spec" ]; then
        echo "Usage: $FUNCNAME file"; return
    fi
    spec_get_developer_email "$spec"
}

