#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

test_config_get() {
    ut_assert_eq "maminjie" "$(config_get $(__config_file) gitee id)"
}

# test_config_set file
test_config_set() {
    local file="$1"
    if [ ! -f "$file" ]; then
        echo "usage: test_config_set file"; return
    fi
    config_set $file "test" "k1" "v1"
    ut_assert_eq "v1" "$(config_get $file "test" "k1")"
}
