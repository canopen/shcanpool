#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

# test_func owner repos number
test_check_pr_compile() {
    if [ $# -ne 3 ]; then
        echo "usage: test_check_pr_compile owner repos number"; return
    fi
    local owner="$1"
    local repos="$2"
    local number="$3"

    gec_init
    for repo in $repos; do
        echo -n "check PR compile https://gitee.com/$owner/$repo/pulls/$number : "
        gitee_check_pr_compile "$owner" "$repo" "$number"
        local res=$?
        if [ $res -eq 0 ]; then
            color_print "$CLR_GREEN" "success"
        elif [ $res -eq 1 ]; then
            color_print "$CLR_RED" "failed"
        elif [ $res -eq 2 ]; then
            color_print "$COLOR_YELLOW" "warning"
        elif [ $res -eq 3 ]; then
            color_print "$CLR_RED_WHITE" "error"
        fi
    done
}

