<!-- TOC -->

- [1. 简介](#1-简介)
- [2. 目录](#2-目录)
- [3. 使用](#3-使用)
    - [3.1. token 配置](#31-token-配置)
    - [3.2. 命令自动补全](#32-命令自动补全)
- [4. 教程](#4-教程)
- [5. 结语](#5-结语)

<!-- /TOC -->

# 1. 简介
ola（哦啦），对，不是 euler（欧拉） -_-

ola 实际上是作者为 openEuler 写的一个 shell 脚本框架。

ola 原始仓库为 [https://gitee.com/maminjie/ola](https://gitee.com/maminjie/ola) , 原仓不再更新。

由于 ola 中的通用脚本越来越丰富，于是作者就将其提取成更通用的脚本框架 shcanpool，也就是本仓库。

ola 和 shcanpool 随后便面临着同时维护，相互间就出现了要不断同步的问题，所以就将 ola 集成到了 shcanpol 中，作为一个 project 向前演进，随着 ola 的不断完善，也可以丰富 shcanpool 框架，二者相辅相成。

# 2. 目录

|目录|说明|
|:---|:---|
|ola.sh|命令入口|
|apps|应用（命令）脚本|
|config|配置文件模板|
|dist|发布相关文件|
|libs|库脚本，基于标准命令实现|
|plugins|插件（命令）脚本|
|test|测试脚本|

由于 ola 集成到了 shcanpool 中，所以 ola 不再是一个独立的框架，它变成了 shcanpool 的一个工程，所以 ola 中的脚本需要依赖 shcanpool 框架中的脚本。

# 3. 使用

ola 的开发和使用就像 shcanpool 的 demo 工程用法一样，可以查看 shcanpool 的 [README.md](https://gitee.com/icanpool/shcanpool/blob/master/README.md)，将 README 中的 demo 换成 ola 即可，此处不再赘述，这里只讲 ola 中的特定功能。

## 3.1. token 配置
由于 openEuler 的仓库是搭建在 gitee 平台上，所以就难免要和 gitee 平台做交互，主要是基于 gitee 的 [OpenAPI](https://gitee.com/api/v5/swagger)，因此就需要配置 access token。目前有两种方法生成 token：

1、通过第三方应用(不推荐)，[OAuth文档](https://gitee.com/api/v5/oauth_doc#/list-item-2)

2、通过私人令牌。在个人账户设置中找到[私人令牌](https://gitee.com/profile/personal_access_tokens)，生成新令牌即可。

其中，第三方应用的方式获取的AccessToken，有效期为一天，而私人令牌生成的AccessToken没有时间限制。

另外，access_token 有访问权限范围，根据具体情况选择：user_info projects pull_requests issues notes keys hook groups gists enterprises


通过 `ola init` 初始化配置文件，然后通过 `ola set` 配置 token 和 码云id，如下所示：
```shell
[gitee]
token = xxxxx
id = maminjie
```

## 3.2. 命令自动补全
执行 `source /xx/yy/shcanpool/projects/ola/completion/ola` 然后 shell 终端键入 ola 命令加空格，然后按两次 TAB 键可以自动调出来相关的子命令，如下所示：
```shell
[maminjie@fedora canpool]$ ola [TAB][TAB]
abi          debug        init         packageinit  setting      sync
call         gec          nvd          patcher      ship         uncompress
checkenv     git          obs          repo         sig          unittest
cloc         help         oec          rpm          spec         yumpkg
[maminjie@fedora canpool]$ ola o[TAB][TAB]
obs  oec
[maminjie@fedora canpool]$ ola oec [TAB][TAB]
finddeveloper  help           patchinfo      release
[maminjie@fedora canpool]$ ola -[TAB][TAB]
-h         --help     -v         --version
```

# 4. 教程

- [CSDN](https://blog.csdn.net/canpool/category_10640960.html)

# 5. 结语
更多功能请通过 `ola -h` 自行解锁！
