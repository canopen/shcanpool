Name:                ola
Version:             0.0.5
Release:             1
Summary:             A fancy shell command-line tools framework for openEuler
License:             MulanPSL-2.0
URL:                 https://gitee.com/maminjie/ola
Source0:             https://gitee.com/maminjie/%{name}/repository/archive/v%{version}.tar.gz
BuildArch:           noarch
BuildRequires:       bash
Requires:            rpm rpm-build rpmlint dnf yum osc libabigail
Requires:            bash git vim-enhanced tar unzip util-linux expect
%description
A fancy shell command-line tools framework for openEuler

%prep
%autosetup -n %{name} -p1

%build

%install
mkdir -p %{buildroot}%{_datadir}/ola/
cp -r ola.sh config src test %{buildroot}%{_datadir}/ola

%check
bash ola.sh ut -a

%post
ln -s %{_datadir}/ola/ola.sh %{_bindir}/ola

%postun
rm -f %{_bindir}/ola

%files
%doc README.*
%license LICENSE
%{_datadir}/ola/*

%changelog
* Tue Jun 15 2021 maminjie <canpool@163.com> - 0.0.5-1
- package init
