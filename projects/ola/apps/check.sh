#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

tar_docheck() {
    check_command "tar"
}

unzip_docheck() {
    check_command "unzip"
}
