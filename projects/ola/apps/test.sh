#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

method_def unittest

usage_unittest() {
printf "unittest (ut): unit test

Usage:
    ${PROG} ut ...
\n"
}

alias_def unittest ut
do_unittest() {
    local ut_entry="$PROJECT_DIR/test/test.sh"
    if [ -f "$ut_entry" ]; then
        bash $ut_entry "$@"
    else
        echo "No such file $ut_entry"
    fi
}

