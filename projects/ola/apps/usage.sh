#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

usage_notes() {
printf "Notes:
    command-param: If the parameter of the command is plural, then this parameter can be a
        string list (space separated, as: \"a b\") or a list file (one per line or space separated).
        see the function \"get_list\" for more.
"
}

usage() {
    echo "Usage: ${PROG} [GLOBALOPTS] SUBCOMMAND [OPTS] [ARGS...]"
    echo "or: ${PROG} help SUBCOMMAND"
    echo ""
    echo "${DESC}"
    echo "Type '${PROG} help <subcommand>' for help on a specific subcommand."
    echo ""
    echo "Commands:"
    usage_commands
    echo ""
    echo "Global Options:"
    echo "    -h, --help          Show this help message and exit"
    echo "    -v, --version       Get version of ${PROG} and exit"
    echo ""
    usage_notes
}

do_version() {
    if [ -n "$VERSION" ]; then
        echo "$PROG $VERSION"
        echo "Copyright (c) 2021 maminjie <canpool@163.com>"
        echo "License: MulanPSL-2.0 (https://license.coscl.org.cn/MulanPSL2)"
        echo "Contact https://gitee.com/icanpool/shcanpool/ - projects/ola"
    fi
}

do_usage() {
    local arg=$1

    case $arg in
        "-v"|"--version")
            do_version; exit
            ;;
        *)
            echo "$(usage)"; exit
            ;;
    esac
}
