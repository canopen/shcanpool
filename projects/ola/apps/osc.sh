#!/usr/bin/env bash
# Copyright (c) 2021 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

# osc_update
osc_update() {
    local service=_service

    if [ ! -f "$service" ]; then
        return
    fi

    if [ $(ls $service* | wc -l) -eq 1 ]; then
        osc up -S 2> /dev/null
    fi

    rm -f $service *.obscpio *.obsinfo

    for file in $(ls); do
        local new_file=${file##*:}
        if [ "$file" != "$new_file" ]; then
            mv "$file" "$new_file"
        fi
    done
}

osc_config() {
    local config_file=$HOME/.oscrc
    if [ ! -f "$config_file" ]; then
        printf "[general]
apiurl=http://117.78.1.88
no_verify = 1
build-root = /home/maminjie/osc/buildroot

[http://117.78.1.88]
user =
pass =
" > $config_file
    fi
    vi $config_file
}

osc_docheck() {
    check_command "osc"
}
