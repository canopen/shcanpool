#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

readonly PROG="ola"
readonly DESC="automatic work tools."
readonly VERSION="0.0.6"
readonly CONFIG_FILE="$HOME/.$PROG.conf"
