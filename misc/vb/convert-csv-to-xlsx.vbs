' Copyright (c) 2022 maminjie <canpool@163.com>
' SPDX-License-Identifier: MulanPSL-2.0

Sub convert_csv_to_xlsx()
    Dim fDir As String
    Dim wB As Workbook
    Dim wS As Worksheet
    Dim fPath As String
    Dim sPath As String
    fPath = "C:\Users\Micro\Desktop\source\"
    sPath = "C:\Users\Micro\Desktop\target\"
    fDir = Dir(fPath)
    Do While (fDir <> "")
        If Right(fDir, 4) = ".csv" Then
            On Error Resume Next
            Set wB = Workbooks.Open(fPath & fDir)
            Dim temp As String
            temp = Left(fDir, Len(fDir) - 4)
            For Each wS In wB.Sheets
                wS.SaveAs sPath & temp & ".xlsx" _
                , FileFormat:=xlOpenXMLWorkbook, CreateBackup:=False
            Next wS
            wB.Close False
            Set wB = Nothing
        End If
        fDir = Dir
        On Error GoTo 0
    Loop
End Sub
