' Copyright (c) 2022 maminjie <canpool@163.com>
' SPDX-License-Identifier: MulanPSL-2.0

''
' Usage:
'   Convert csv to excel, each csv as a sheet
'   1. Create a new excel file in the csv file directory
'   2. Right click on the sheet to view the code
'   3. Copy the current code and click execute
''
Private Sub copy_csv_to_excel()
    Dim myPath$, myFile$, AK As Workbook
    Application.ScreenUpdating = False
    myPath = ThisWorkbook.Path & "\"
    myFile = Dir(myPath & "*.csv")
    Do While myFile <> ""
        If myFile <> ThisWorkbook.Name Then
            Set AK = Workbooks.Open(myPath & myFile)
            AK.Sheets(1).Copy After:=ThisWorkbook.Sheets(ThisWorkbook.Sheets.Count)
            Workbooks(myFile).Close
        End If
        myFile = Dir
        Set AK = Nothing
    Loop
    Application.ScreenUpdating = True
    ActiveWorkbook.Save
End Sub
