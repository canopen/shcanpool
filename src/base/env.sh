#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

[[ -n "$SH_OPTION" ]] && set -$SH_OPTION
