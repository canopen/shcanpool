#!/usr/bin/env bash
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

# math_set_d f1 f2
#   calculate the diffrence set of file f1 and f2
math_set_d() {
    sort $1 $2 $2 | uniq -u
}

# math_set_u f1 f2
#   calculate the union set of file f1 and f2
math_set_u() {
    sort $1 $2 | uniq
}

# math_set_n f1 f2
#   calculate the intersection set of file f1 and f2
math_set_n() {
    sort $1 $2 | uniq -d
}
