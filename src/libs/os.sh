#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0


os_ncpu() {
    grep "processor" /proc/cpuinfo | wc -l
}