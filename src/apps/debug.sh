#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

method_def debug

usage_debug() {
printf "debug (d): debug the subcommad

usage:
    ${PROG} d SUBCMD [ARGS]

notes:
    The SUBCMD does not contain debug command
\n"
}

alias_def debug d
# do_debug SUBCMD [ARGS]
do_debug() {
    if [ $# -lt 1 ]; then
        usage_debug; exit
    fi
    local subcmd=$1

    subcmd=$(alias_fullname "$subcmd")
    if [ "$subcmd" = "debug" ]; then
        echo "The debug command is not supported"; exit
    fi
    if [ "$(method_exists "$subcmd")" ]; then
        shift
        set -x
        do_$subcmd "$@"
        set +x
    else
        echo "\"$subcmd\" is not the supported command"
    fi
}
