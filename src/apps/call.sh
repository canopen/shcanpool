#!/usr/bin/env bash
# Copyright (c) 2022 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

method_def call

usage_call() {
printf "call (c): Call the inner function

usage:
    ${PROG} c FUNCTION [ARGS]
    ${PROG} c -l                # list all inner functions
\n"
}

alias_def call c
# do_call FUNCTION [ARGS]
do_call() {
    if [ $# -lt 1 ]; then
        usage_call; exit
    fi
    if [ "$1" = "-l" ]; then
        declare -F | grep -v "\-fx" | awk '{print $3}'
        return 0
    fi
    local func="$1"
    if [ "$(type -t "$func")" = "function" ]; then
        shift
        $func $@
    else
        echo "\"$func\" is not the inner function"
    fi
}
