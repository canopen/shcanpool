#!/usr/bin/env bash
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0

readonly CUR_DIR=$(dirname $(readlink -f "$0"))

readonly SHCANPOOL_DIR="$CUR_DIR/.."

# config $HOME/bin
# echo 'export PATH="$HOME/bin:$PATH"' >> ~/.bashrc

# init config files
cp $CUR_DIR/conf/{.vimrc,.tmux.conf} $HOME

bash $SHCANPOOL_DIR/projects/ola/ola.sh init
bash $SHCANPOOL_DIR/projects/ola/ola.sh git alias

