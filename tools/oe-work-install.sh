#!/usr/bin/env bash
# Copyright (c) 2024 maminjie <canpool@163.com>
# SPDX-License-Identifier: MulanPSL-2.0


# install and run gnome desktop
dnf group install GNOME -y
systemctl set-default graphical.target
systemctl isolate graphical.target

# install tools
dnf install vim git fish tmux perl -y

# install vmware-tools
# ./vmware-install.pl -d
