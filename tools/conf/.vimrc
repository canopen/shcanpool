syntax on
set number
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set showmatch
set showmode
set showcmd
set incsearch
set autoindent
set smartindent
set autoread
set hlsearch

set wildmenu
set wildmode=longest:list,full

filetype plugin indent on
